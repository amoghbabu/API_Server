const express = require('express');
const app = express.Router();

//importing models
//const user = require('../models/user');

/*
Details: Route is used to get user details
Route: `user`
Method: GET
Parameters: null :Body
Response: Welcome message 
*/
const { getTasks, createTask, readTask, deleteTask, updateTask, getTask } = require('../controllers/todoController');
// our Routes
app.get("/use", (req, res) => {
    res.status(200).json({
        message: "hsagmvgshda"
    })
})

app.route('/tasks')
    .get(getTasks)
    .post(createTask);
app.route('/tasks/:taskId')
    .get(getTask)
    .get(readTask)
    .put(updateTask)
    .delete(deleteTask);
module.exports = app;