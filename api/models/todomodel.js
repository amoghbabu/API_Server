const mongoose = require('mongoose');
const TaskSchema =  mongoose.Schema({
    name: {
        type: String,
        Required: 'Task label is required!'
    },
    description:{
        type: String,
        Required: 'Task Label is required!'
    },
    status: {
        type: [{
            type: String,
            enum: ['completed', 'ongoing', 'pending']
        }],
        default: ['pending']
    }
},{
    timestamps:true
});
module.exports = mongoose.model('task', TaskSchema);