const http = require("http");
const app = require("./app");
const config = require("./config/config.json");
//TODO: Make use of forever npm library and cluster based programming for fast and reliable application
const PORT = config._PORT || 3200;
const Task = require("./api/models/todomodel");
const bodyParser = require('body-parser');
const server = http.createServer(app);

//console.log("server started")
server.listen(PORT);
console.log('App running on ' + PORT);

// handle incoming requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// middleware to handle wrong routes 
app.use( (req, res) => {
    res.status(404).send({ url: req.originalUrl + 'not found' });
});

// register our routes
let routes = require("./api/routes/todoRoute");
app.use("/api",routes);